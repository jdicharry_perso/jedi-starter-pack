<?php

/**
 * The Template for the front-page
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Timber\Timber;

class FrontPageController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = $context['posts'][0];

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;

        return new TimberResponse('templates/front-page.twig', $context);
    }
}
