<?php

namespace App\PostTypes;

use Rareloop\Lumberjack\Page;

class Exemple extends Post
{
    /**
     * Return the key used to register the post type with WordPress
     * First parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return string
     */
    public static function getPostType()
    {
        return 'produit';
    }

    /**
     * Return the config to use to register the post type with WordPress
     * Second parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array
     */
    protected static function getPostTypeConfig()
    {
        return [
            'labels' => [
                'name' => __('Exemples'),
                'singular_name' => __('Exemple'),
            ],
            'public' => true,
            'menu_position'=> 5,
            'has_archive' => true,
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-admin-page',
            'supports' => [
                'title',
                'editor',
                'thumbnail',
                'excerpt',
                'author',
                'revisions',
            ],
            'rewrite' => [
                'slug' => __('exemples'),
            ],
        ];
    }

    // public function children($post_type = 'any', $childPostClass = false)
    // {
    //     $post_type = self::getPostType();
    //     return parent::children($post_type);
    // }
}
