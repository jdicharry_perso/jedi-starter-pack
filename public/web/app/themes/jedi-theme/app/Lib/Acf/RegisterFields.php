<?php

namespace App\Lib\Acf;

use Timber\Timber;
use Rareloop\Lumberjack\Facades\Config;

class RegisterFields
{
    private $settings;

    public function __construct(Array $settings)
    {
        $this->settings = $settings;
        $this->load();
    }

    public function load()
    {
        add_action('acf/init', [$this, 'registerOptionsPage']);
        add_action('acf/init', [$this, 'registerBlockType']);

        // Only allow fields to be edited on development
		if ( Config::get('app.environment') !== 'development' ) {
			add_filter( 'acf/settings/show_admin', '__return_false' );
		}

        if($this->settings['autoloading']) {
            $this->registerFields();
        }
    }

    private function registerFields()
    {
        $_this = $this;

        if( strtoupper($this->settings['type_loading_fields']) === 'PHP') {
            add_action('acf/init', function() use ($_this) {
                $_this->registerPhpFields($_this->settings['fields']);
            });
        } elseif( strtoupper($this->settings['type_loading_fields']) === 'JSON') {
            if( Config::get('app.environment') == 'development' ) {
                add_filter('acf/settings/save_json', function($path) use ($_this) {
                    return $_this->saveJsonFields($_this->settings['fields']);
                });
            // } else {
            //     add_action('acf/load_field_groups', function($groups) use ($_this) {
            //         $_this->privatizeJsonFields($groups);
            //     });
            }


            add_filter('acf/settings/load_json', function($path) use ($_this) {
                return $_this->registerJsonFields($path, $_this->settings['fields']);
            });
        }
    }

    private function registerPhpFields(Array $settings)
    {
        if (function_exists('acf_add_local_field_group')) :
            foreach ($settings as $setting) {
                acf_add_local_field_group($setting);
            }
        endif;
    }

    private function saveJsonFields(String $path) : String
    {
        return $path;
    }

    private function registerJsonFields(Array $paths, String $path) : Array
    {
        unset($paths[0]);
        $paths[] = $path;
        return $paths;
    }

    private function privatizeJsonFields($groups)
    {
        $groups = array_map(function($group) {
            $group['private'] = true;
            return $group;
        }, $groups);

        return $groups;
    }

    public function registerOptionsPage()
    {
        if (function_exists('acf_add_options_page')) :
            if (isset($this->settings['options']) && !empty($this->settings['options'])) {
                foreach ($this->settings['options'] as $option) {
                    acf_add_options_page($option);
                }
            }
        endif;
    }

    public function registerBlockType()
    {
        if (function_exists('acf_register_block_type')) :
            if (isset($this->settings['block_type']) && !empty($this->settings['block_type'])) {
                foreach ($this->settings['block_type'] as $blockType) {
                    acf_register_block_type($blockType);
                }
            }
        endif;
    }
}
